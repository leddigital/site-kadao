<?php

namespace App\Http\Controllers;

use Symfony\Component\HttpFoundation\Request;
use App\Upload;
use Storage;
use Illuminate\Filesystem\Filesystem;
use Symfony\Component\HttpFoundation\File;


class FileController extends Controller
{

  /**
   * Moves the file to a new location.
   *
   * @param string $directory The destination folder
   * @param string $name      The new file name
   *
   * @return File A File object representing the new file
   *
   * @throws FileException if, for any reason, the file could not have been moved
   */

    /**
     * Returns the original file name.
     *
     * It is extracted from the request from which the file has been uploaded.
     * Then it should not be considered as a safe value.
     *
     * @return string|null The original name
     */
    /**
    * Store a newly created resource in storage.
    *
    * @param  \Illuminate\Http\Request  $request
    * @return \Illuminate\Http\Response
    */
  public function store(Request $request)
  {
    //Define um valor default para a variavel
    $namefile = null;

    //Verifica se o arquivo e valido
    // if($request->hasfile('image') && $request->file('image')->isValid()){

      $name = uniqid(date('HisYmd'));//Define um nome aleatorio para o arquivo baseado no timestamps
      $extension = $request->image->extension();//Recupera a extenção do arquivo
      $namefile = "{$name}.{$extension}";//Define o nome final do arquivo
      $upload =$request->image->storeAs('pdf', $namefile);//Faz o Upload
      //Se tiver funcionando ele foi armazenado em storage/app/public/pdf/arquivo.pdf

      //Verifica se deu algum tipo de erro 
      if(!$upload){
        return redirect()
                      ->back()
                      ->with('Erro, Falha no upload')
                      ->withInput();
      }
      echo 'Salvo com sucesso';
    
  }
}
