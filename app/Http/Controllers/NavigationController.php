<?php

namespace App\Http\Controllers;

use App\Banners;
use App\Categories;
use App\categories_has_contents;
use App\Contents;
use App\ContentsCategories;
use App\Informations;
use Illuminate\Http\Request;
use App\Mail\SendMail;
use Symfony;
use Illuminate\Contracts\Validation\Validator;
use Symfony\Component\Console\Input\Input;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Redirect;
use Symfony\Component\HttpFoundation\File;

class NavigationController extends Controller
{

    public function index(Request $request)
    {
        $banners = Banners::all();

        $down = Contents::where('type', '=', 'catalogo')->get()->first();

        return view('pages.home', ['banners' => $banners, 'down' => $down]);
    }
    public function qualidade()
    {
        return view('pages.qualidade');
    }
    public function empresa()
    {
        return view('pages.empresa');
    }
    public function presenca()
    {
        return view('pages.presenca');
    }
    public function produtos(Request $request)
    {
        // $url = $request->route('url');
        //faz a consulta de produtos no banco 
        $produto = Categories::where('type', '=', 'produtos')->get();

        return view('pages.produtos', ['produto' => $produto]);
    }
    
    public function produto(Request $request)
    {
        //Pega a URL da rota
        $url = $request->route('url');
        //Faz uma consulta e pega os dados no banco 
        $cat = Categories::where('url', '=', $url)->get()->first();
        //Chama a função na controller contents para pegar somente o produto com a categoria cadastrada
        $produtos = $cat->contents()->get();
      


        return view('pages.interna.produto', [ 'cat' => $cat, 'produtos' => $produtos]);
    }

    public function produtoinf(Request $request)
    {
        //Pega a URL da rota
        $url = $request->route('url');
        //Faz uma consulta e pega os dados no banco 
        $product = Contents::with('categories')->get()->first();
        
        return view('pages.interna.produtoinfo', ['product' => $product]);
    }

    public function sobre(Request $request)
    {
        $page = Contents::where('url', 'sobre-o-filme')->get()->first();
        return view(
            'pages.sobre',
            ['page' => $page]
        );
    }
    public function exiba(Request $request)
    {
        $page = Contents::where('url', 'exiba-o-filme')->get()->first();
        return view(
            'pages.exiba',
            ['page' => $page]
        );
    }
    public function calendario(Request $request)
    {
        $page = Contents::where('url', 'agenda-de-exibicoes')->get()->first();
        return view(
            'pages.agenda',
            ['page' => $page]
        );
    }

    public function festivais(Request $request)
    {
        $page = Contents::where('url', 'festivais')->get()->first();
        return view(
            'pages.festivais',
            ['page' => $page]
        );
    }

    public function noticias(Request $request)
    {
        $page = Contents::where('url', 'noticias')->get()->first();
        return view(
            'pages.noticias',
            ['page' => $page]
        );
    }

    public function mobilizacao(Request $request)
    {
        $page = Contents::where('url', 'mobilizacao-social')->get()->first();
        return view(
            'pages.mobilizacao',
            ['page' => $page]
        );
    }

    public function tese(Request $request)
    {
        $page = Contents::where('url', 'tese-doutorado')->get()->first();
        return view(
            'pages.tese',
            ['page' => $page]
        );
    }

    public function contato(Request $request)
    {
        $page = Contents::where('url', 'contato')->get()->first();
        return view(
            'pages.contato',
            ['page' => $page]
        );
    }

    public function blog(Request $request)
    {
        $url = $request->route('url');
        // $category = Categories::all();
        $page = Contents::with("categories")->where('type', '=', 'blog')->where('url', '=', $url)->get()->first();

        return view(
            'pages.blog',
            ['page' => $page]
        );
    }

    public function mail(Request $request)
    {
        $form = $request->all();
        Mail::to(['edinaldo@agencialed.com.br'])
            ->send(new SendMail($form,'Formulário de Contato'));
        if (Mail::failures()) {
            echo '0';
            exit;
        }
        echo '1';
    }
}
