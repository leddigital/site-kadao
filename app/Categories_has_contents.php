<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class categories_has_contents extends Model
{
    public $timestemps = true;

    protected $fillable = [
        'categories_id',
        'contents_id'
    ];

    public function categories_has_contents(){
        return $this->hasToMany('App\Categories', 'App\Contents');
    }
}
