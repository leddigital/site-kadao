<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Files extends Model
{
    public function user(){
        return $this->belongsTo(User::class);
    }

    public function upload(){
        return 'x';
    }
}
