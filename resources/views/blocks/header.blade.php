<header>
    <div class="container-fluid p-0" id="top">
        <nav class="navbar navbar-expand-lg p-0 navbar-light">
            <span class="navbar-brand">
                <a href="{{route('nav.index')}}">
                    <img src="{{asset('img/logo.png')}}" alt="Kadão Alimentos" class="img-responsive">
                </a>
            </span>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#basic-navbar-nav"
                aria-controls="basic-navbar-nav" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="navbar-collapse collapse show" id="basic-navbar-nav">
                <div class="ml-auto navbar-nav">
                    <a href="{{route('empresa')}}" class="nav-link {{ (\Request::route()->getName() == 'empresa') ? 'active' : '' }} pulse text-white">
                        Empresa
                    </a>
                    <a href="{{route('qualidade')}}" class="nav-link {{ (\Request::route()->getName() == 'qualidade') ? 'active' : '' }} pulse text-white">
                        Qualidade
                    </a>
                    <a href="{{route('presenca')}}" class="nav-link {{ (\Request::route()->getName() == 'presenca') ? 'active' : '' }} pulse text-white">
                        Presença
                    </a>
                    <a href="{{route('produtos')}}" class="nav-link {{ (\Request::route()->getName() == 'produtos') ? 'active' : '' }} pulse text-white">
                        Produtos
                    </a>
                </div>
            </div>
        </nav>
    </div>
    {{-- <nav class="navbar navbar-expand-lg navbar-light">
        <a href="{{route('nav.index')}}">
    <img src="{{asset('img/logo.png')}}" alt="Kadão Alimentos" class="img-responsive">
    </a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarTogglerDemo02"
        aria-controls="navbarTogglerDemo02" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="navbarTogglerDemo02">
        <ul class="navbar-nav mr-auto mt-2 mt-lg-0">
            <div class="ml-auto navbar-nav show">
                <a href="{{route('empresa')}}" class="nav-link pulse text-white">
                    Empresa
                </a>
                <a href="{{route('qualidade')}}" class="nav-link pulse text-white">
                    Qualidade
                </a>
                <a href="{{route('presenca')}}" class="nav-link pulse text-white">
                    Presença
                </a>
                <a href="{{route('produtos')}}" class="nav-link pulse text-white">
                    Produtos
                </a>
            </div>
        </ul>
    </div>
    </nav> --}}
</header>