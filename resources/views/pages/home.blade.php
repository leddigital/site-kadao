@extends('layouts.default')

@section('title')
    Kadão Alimentos
@endsection

@section('content')
<section id="slider">
    <div class="container-fluid p-0">
        <div id="carouselExampleControls" class="carousel slide" data-ride="carousel">
            <div class="carousel-inner">
                @foreach ($banners as $item)
                <div class="carousel-item {{$loop->index=='1'?'active':''}} ">
                    <img class="d-block w-100" src="{{asset('banners/'.$item->image)}}" alt="First slide">
                </div>
                @endforeach
                <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
                    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                    <span class="sr-only">Previous</span>
                  </a>
                  <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
                    <span class="carousel-control-next-icon" aria-hidden="true"></span>
                    <span class="sr-only">Next</span>
                  </a>
            </div>
        </div>
    </div>
</section>
<section id="sobre">
    <div class="container w-100">
        <div class="row">
            <div class="text-center col-sm-12  col-md-8 offset-md-2" id="txt-apre">
                <h2 class="h2 font-orange">A KADÃO ALIMENTOS</h2>
                <p>
                    A Kadão Alimentos nasceu em 2010, após um longo período de atuação de seus fundadores no
                    agronegócio. Motivados pelo interesse em processamento de carne bovina, a empresa tem marcado seu
                    nome neste mercado com produtos de alta qualidade, controle rigoroso de processos e a busca
                    constante pela total satisfação de seus clientes. Com uma administração familiar, a Kadão sabe o que
                    o brasileiro deseja colocar em sua mesa: um alimento de ótimo custo-benefício, com garantia de
                    qualidade e procedência.
                </p>
                <a href="{{route('empresa')}}" class="mt-4 bg-orange font-white btn btn-primary">CONHEÇA MAIS</a>
            </div>
        </div>
    </div>
</section>
<section id="qualidade">
    <div class="container-fluid h-100">
        <div class="row h-100">
            <div class="text-center my-auto col-12">
                <h2 class="h2 font-white font-500">
                    QUALIDADE
                </h2>
                <p class="font-white">
                    Todos os produtos Kadão passam por métodos e avaliações eficientes,
                    <br>
                    capazes de garantir a procedência total dos alimentos. Essa qualidade é
                    <br>
                    percebida por todos os consumidores.
                </p>
                <a href="{{route('qualidade')}}" class="bg-orange mt-3 font-white btn btn-primary">COMPROVE</a>
            </div>
        </div>
    </div>
</section>
<section id="presenca">
    <div class="container-fluid h-100">
        <div class="row h-100">
            <div class="text-center my-auto col-12">
                <h2 class="h2 font-orange font-500">
                    PRESENÇA
                </h2>
                <p>
                    Marca forte aqui no Brasil e lá fora pelo mundo.
                    <br>
                    O zelo na produção tornou a Kadão uma indústria de
                    <br>
                    referência e presença internacional.
                </p>
                <a href="{{route('presenca')}}" class="bg-orange mt-1 font-white btn btn-primary">EXPLORE</a>
            </div>
        </div>
    </div>
</section>
<section class="catalogo bg-orange">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-8 offset-md-2">
                <div class="row">
                    <div class="col-sm-4">
                        <div class="row text-center" id="down">
                            <div class="col-md-4">
                                <img src="{{asset('img/icon.png')}}" id="icon-down">
                            </div>
                            <div class="col-sm-8 text-center">
                                <a href="{{asset('pdf/'.$down->image)}}" download="Catalogo" class="text-decoration-none hover">
                                    <p class="font-white" id="download">
                                        <span class="font-500" >Baixe aqui</span>
                                        <br>
                                        nosso catálogo
                                        <br>
                                        de produtos
                                    </p>
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-8" >
                        <img id=" img-download" src="{{asset('img/product.png')}}" class="img-products img-fluid">
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<section id="nosso-produto">
    <div class="container-fluid h-100">
        <div class="row h-100">
            <div class="text-center my-auto col-12">
                <h2 class="h2 font-white">NOSSOS PRODUTOS</h2>
                <p class="font-white">
                    Feitos com amor e máxima atenção. Os produtos Kadão são sinônimos de
                    <br>
                    procedência e qualidade. Atributos que são acompanhados de um cuidado
                    <br>
                    diferenciado e máximo respeito pelo alimento.
                </p>
                <a href="{{route('produtos')}}" class="bg-orange mt-3 font-white btn btn-primary">CONHEÇA MAIS</a>
            </div>
        </div>
    </div>
</section>
<section id="contato">
    <div class="container-fluid my-3">
        <div class="titulo-contato text-center">
            <h2 class="font-orange">ENTRE EM CONTATO CONOSCO</h2>
            <h6 class="font-gray">NÃO HESITE EM ENVIAR PERGUNTAS, COMENTÁRIOS E SUGESTÕES</h6>
        </div>
    </div>
    <form action="{{route('mail')}}" method="POST" id="contact-form" class="bg-blue border-ore pt-5 pb-3 border-tp">
        @csrf
        <div class="container">
            <div class="row">
                <div class="text-center col-12">
                    <div class="form text-left">
                        <div class="row">
                            <div class="col-sm-8">
                                <div class="row">
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <div class="input-group">
                                                <input name="nome" type="text" class="form-control form-input"
                                                    placeholder="Nome">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <div class="input-group">
                                                <input name="email" type="text" class="form-control form-input"
                                                    placeholder="E-mail">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <div class="input-group">
                                                <input name="assunto" type="text" class="form-control form-input"
                                                    placeholder="Assunto">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <div class="input-group">
                                                <input name="telefone" type="text" class="form-control form-input"
                                                    placeholder="Telefone">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-12">
                                        <div class="form-group">
                                            <div class="input-group">
                                                <textarea id="messagemContato" name="mensagem" placeholder="Menssagem"
                                                    rows="12" class="form-control"></textarea>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="text-left text-secondary col-sm-8">
                                        <p>
                                            Todos os campos são obrigatórios para o envio.
                                        </p>
                                    </div>
                                    <div class="text-right col-sm-4">
                                        <button class="form-input btn-env bg-orange btn btn-primary font-white" type="submit">Enviar</button>
                                    </div>
                                </div>
                            </div>
                            <div class="text-center col-sm-4">
                                <img src="{{asset('img/sac-img.png')}}" alt="sac" class="img-fluid img-sac">
                                <div class="font-500  text-secondary">Horário de Atendimento</div>
                                <div align="center" class=" font-white-light-desc">
                                    De segunda à sexta-feira,
                                    <br>
                                    das 8h00 à 18h00
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
</section>
@endsection