@extends('layouts.default')

@section('title')
    Produtos | Kadão Alimentos
@endsection

@section('content')
<section class="produto">
    <div class="container">
        <div class="row h-100">
            <div class="text-center col-lg-12 mb-5">
                <h1 class="font-orange">
                    {{$product->title}}
                </h1>
            </div>
        </div>
        <div class="row h-100">
            <div class="my-auto text-center col-md-6 mt-3">
                <img src="{{asset('content/'.$product->id.'/'.$product->imagedois)}}" class="img-fluid p-0">
            </div>
            <div class="my-auto text-center col-md-6 mt-3">
                <p>
                    {{$product->description}}
                </p>
            </div>
        </div>
        <div class="row h-100">
            <div class="my-auto text-center col-md-6 mt-3">
                <img src="{{asset('img/prod-peso.jpg')}}" class="img-fluid">
            </div>
            <div class="my-auto text-center col-md-6 mt-3">
                <img src="{{asset('img/gadod+.png')}}" class="img-fluid">
            </div>
        </div>
    </div>
</section>
<section class="tabela-nutricional">
    <div class="row"></div>
    <div class="row"></div>
</section>
@endsection