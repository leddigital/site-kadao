@extends('layouts.default')

@section('title')
    Produtos | Kadão alimentos
@endsection

@section('content')
<section class="produto-lista">
    @foreach ($produtos as $prod)
    <div class="container">
        <div class="row">
            <div class="text-center col-md-4 offset-md-4">
                <h1 class="font-brow">
                   {{$prod->short_description}}
                </h1>
                <p class="font-white py-3">
                    {{$prod->description}}
                </p>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <ul class="img-prod-link animated pulse">
                    <li>
                        <a href="{{route('produto-info',['url','=',$cat->url] )}}">
                            <img src="{{asset('content/'.$prod->id.'/'.$prod->image)}}" class="img-prod img-fluid">
                        </a>
                        <a href="{{route('produto-info',['url','=',$cat->url])}}">
                            <h5 class="mt-3 font-white">
                                {{$prod->title}}
                            </h5>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    @endforeach
</section>
@endsection