@extends('layouts.admin')
@section('title', 'Categoria')
@section('content')
<header class="page-header">
    <h2>Categoria</h2>
    <div class="right-wrapper text-right">
        <ol class="breadcrumbs">
            <li>
                <a href="{{ route('information.index') }}">
                    <i class="fas fa-home"></i>
                </a>
            </li>
            <li><span>Cadastro de Categoria</span></li>
        </ol>
        <a class="sidebar-right-toggle" data-open=""><i class="fas fa-chevron-left"></i></a>
    </div>
</header>
<div class="row">
    <div class="col">
        <form class="form-horizontal form-bordered" id="form_cadastre" method="post"
            action="{{ isset($entity->id)?route('categories.edit.save',['id'=>$entity->id]):route('categories.save') }}">
            @csrf
            <section class="card">
                <header class="card-header">
                    <div class="card-actions">
                        <a href="forms-basic.html#" class="card-action card-action-toggle" data-card-toggle=""></a>
                    </div>
                    <h2 class="card-title">Cadastro de Categorias variadas</h2>
                    <p class="card-subtitle">
                        Preencha o título da categoria e selecione a onde ela pertence.
                    </p>
                </header>
                <div class="card-body" style="display: block;">
                    <div class="row">
                        <div class="col-lg-4">
                            <div class="form-group row">
                                <div class="col-lg-12">
                                    <div class="input-group">
                                        <span class="input-group-prepend">
                                            <span class="input-group-text">
                                                <i class="fas fa-font"></i>
                                            </span>
                                        </span>
                                        <input value="{{ isset($entity->title)?$entity->title:'' }}" type="text"
                                            name="title" class="form-control" placeholder="Título" required>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4">
                            <div class="form-group row">
                                <div class="col-lg-12">
                                    <div class="input-group">
                                        <span class="input-group-prepend">
                                            <span class="input-group-text">
                                                <i class="fas fa-list-ul"></i>
                                            </span>
                                        </span>
                                        <select name="type" class="form-control" required>
                                            <option value=""> --- Selecione um tipo --- </option>
                                            <option
                                                {{ (isset($entity->type) and ($entity->type=='produtos'))?'selected':'' }}
                                                value="produtos">Produto</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>

            <section class="card">
                <header class="card-header">
                    <div class="card-actions">
                        <a href="forms-basic.html#" class="card-action card-action-toggle" data-card-toggle=""></a>
                    </div>
                    <h2 class="card-title">Embalagem do produto</h2>
                    <p class="card-subtitle">
                        Selecione uma imagem como Embalagem do produto.<br />
                        <br />
                        <strong>Obs.: A imagem selecionada será automatimacamente reajustada para o tamanho na descrição
                            do campo.</strong>
                    </p>
                </header>
                <div class="card-body" style="display: block;">
                    <div class="row">
                        <div class="col-lg-12">
                            <small>Tamanho da imagem 490px x 325px</small>
                            <div class="fileupload fileupload-new" data-provides="fileupload">
                                <div class="input-append">
                                    <div class="uneditable-input">
                                        <i class="fas fa-file fileupload-exists"></i>
                                        <span class="fileupload-preview"></span>
                                    </div>
                                    <span class="btn btn-default btn-file">
                                        <span class="fileupload-exists">Trocar</span>
                                        <span class="fileupload-new">Selecionar Imagem</span>
                                        <input type="file" name='banner' accept="image/*" onchange='loadPreview(this, 490,325)'
                                            {{ isset($entity)?'':'' }}>
                                    </span>
                                    <a href="forms-basic.html#" class="btn btn-default fileupload-exists" 167
                                        data-dismiss="fileupload">Remove</a>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-12">
                            <textarea id="base64" name="base64" style="display:none;"></textarea>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="img-content">
                                <img id='output' class='img-fluid'
                                    src='{{ isset($entity->image)!=""?"/cat/".$entity->id."/".$entity->image:'' }}'>
                            </div>
                        </div>
                    </div>
                </div>
            </section>

            <section class="card">
                <div class="card-body" style="display: block;">
                    <button type="submit" class="mb-1 mt-1 mr-1 btn btn-success"><i class="fas fa-save"></i>
                        Salvar</button>
                </div>
            </section>
        </form>
    </div>
</div>
@endsection