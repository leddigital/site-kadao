<div class="sidebar-header">
    <div class="sidebar-title">
        Menu Principal
    </div>
    <div class="sidebar-toggle d-none d-md-block" data-toggle-class="sidebar-left-collapsed" data-target="html"
        data-fire-event="sidebar-left-toggle">
        <i class="fas fa-bars" aria-label="Toggle sidebar"></i>
    </div>
</div>

<div class="nano">
    <div class="nano-content">
        <nav id="menu" class="nav-main" role="navigation">
            <ul class="nav nav-main">
                <li class="nav-active">
                    <a class="nav-link" href="{{ route('information.index') }}">
                        <i class="fas fa-info"></i>
                        <span>Informações Gerais</span>
                    </a>
                </li>
                <li class="nav-parent">
                    <a class="nav-link">
                        <i class="fas fa-align-left" aria-hidden="true"></i>
                        <span>Home</span>
                    </a>
                    <ul class="nav nav-children">
                        <li>
                            <a href="{{route('banners.index')}}">
                                Full Banner
                            </a>
                        </li>
                        <li>
                            <a href="{{route('content.index', ['_type'=>'catalogo'])}}">
                               Upload do catalogo
                            </a>
                        </li>
                    </ul>
                </li>
                <li>
                    <a href="{{route('categories.index', ['_type'=>'categorias'])}}" class="nav-link">
                        <i class="fa fa-barcode"></i>
                        <span>Categorias</span>
                    </a>
                </li>
                <li>
                    <a href="{{route('content.index', ['_type'=>'produtos'])}}" class="nav-link">
                        <i class="fa fa-inbox"></i>
                        <span>Produtos</span>
                    </a>
                </li>
            </ul>
        </nav>
        <hr class="separator" />
    </div>
</div>